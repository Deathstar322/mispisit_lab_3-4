﻿using MISPISIT_lab_3_4.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.Linq;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace MISPISIT_lab_3_4
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        static string connectionString = @"Data Source=localhost\SQLEXPRESS;Initial Catalog=Employees;Integrated Security=True";
        static DataContext database = new DataContext(connectionString);
        // Получаем таблицу пользователей
        Table<Employee> employees = database.GetTable<Employee>();
        

        public MainWindow()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            foreach(Employee emp in employees)
            {
               
                emp.Right = database.GetTable<Rights>().FirstOrDefault(x => x.EmployeeName == emp.Name);
            };
            EmpItemsControl.ItemsSource = employees;
        }

        private void NameSorting_Checked(object sender, RoutedEventArgs e)
        {
            var SortedEmployeeData = employees.OrderBy(x => x.Name);
            EmpItemsControl.ItemsSource = SortedEmployeeData;
            EmpItemsControl.Items.Refresh();
        }

        private void NameSorting_Unchecked(object sender, RoutedEventArgs e)
        {
            Update_DataGrid();
        }
        private void SalarySorting_Unchecked(object sender, RoutedEventArgs e)
        {
            Update_DataGrid();
        }

        private void SalarySorting_Checked(object sender, RoutedEventArgs e)
        {
            var SortedEmployeeData = employees.OrderBy(x => x.Salary);
            EmpItemsControl.ItemsSource = SortedEmployeeData;
            EmpItemsControl.Items.Refresh();
        }

        private void Add_Employee_Click(object sender, RoutedEventArgs e)
        {
            
            Add_Employee add_employee = new Add_Employee();
            add_employee.Show();
        }

        public void Update_DataGrid()
        {
            string connectionString = @"Data Source=localhost\SQLEXPRESS;Initial Catalog=Employees;Integrated Security=True";
            DataContext database = new DataContext(connectionString);
            Table<Employee> new_employees = database.GetTable<Employee>();
            foreach (Employee emp in new_employees)
            {
                emp.Right = database.GetTable<Rights>().FirstOrDefault(x => x.EmployeeName == emp.Name);
            };
            EmpItemsControl.ItemsSource = new_employees;
            EmpItemsControl.Items.Refresh();
        }

        private void Delete_Employee_Click(object sender, RoutedEventArgs e)
        {
            Button button = sender as Button;
            int emp_id = int.Parse(button.MaxHeight.ToString());
            Employee emp = database.GetTable<Employee>().FirstOrDefault(x => x.Id == emp_id);
            if(emp == null)
            {
                MessageBox.Show("Выберите сотрудника");
            }
            else
            {
                database.GetTable<Employee>().DeleteOnSubmit(emp);
                database.SubmitChanges();
                database.Refresh(RefreshMode.KeepCurrentValues, employees);
                Emp_Rights emp_rights = database.GetTable<Emp_Rights>().FirstOrDefault(x => x.EmployeeId == emp.Id);
                int rights_id = database.GetTable<Emp_Rights>().FirstOrDefault(x => x.EmployeeId == emp.Id).RightsId;
                Rights rights = database.GetTable<Rights>().FirstOrDefault(x => x.Id == rights_id);
                database.GetTable<Rights>().DeleteOnSubmit(rights);
                database.SubmitChanges();
                database.GetTable<Emp_Rights>().DeleteOnSubmit(emp_rights);
                database.SubmitChanges();
                Table<Employee> new_employees = database.GetTable<Employee>();
                foreach (Employee empl in new_employees)
                {
                    empl.Right = database.GetTable<Rights>().FirstOrDefault(x => x.EmployeeName == emp.Name);
                };
                EmpItemsControl.ItemsSource = new_employees;
                EmpItemsControl.Items.Refresh();
            }
           
        }

        private void Employee_Rights_Button_Click(object sender, RoutedEventArgs e)
        {
            Button button = sender as Button;
            int emp_id = int.Parse(button.MaxHeight.ToString());
            Employee emp = database.GetTable<Employee>().FirstOrDefault(x => x.Id == emp_id);
            if (emp == null)
            {
                MessageBox.Show("Выберите сотрудника");
            }
            else
            {
                Employee_Rights add_employee = new Employee_Rights(emp);
                add_employee.Show();
            }

        }

        private void Update_DataGrid(object sender, RoutedEventArgs e)
        {
            Update_DataGrid();
        }
    }
}
