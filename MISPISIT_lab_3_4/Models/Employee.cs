﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.Linq.Mapping;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace MISPISIT_lab_3_4.Models
{
    [Table(Name = "Employees")]
    internal class Employee : INotifyPropertyChanged
    {

        private int _salary;
        private string _name;
        private string _position;
        [Column(IsPrimaryKey = true, IsDbGenerated = true)]
        public int Id { get; set; }
        [Column(Name = "HireDate")]
        public DateTime HireDate { get; set; } = DateTime.Now;
        [Column(Name = "Name")]
        public string Name
        {
            get { return _name; }
            set
            {
                if(_name == value)
                {
                    return;
                }
                _name = value;
                OnPropertyChanged("Name");
            }
        }
        [Column(Name = "Salary")]
        public int Salary
        {
            get { return _salary; }
            set
            {
                if (_salary == value)
                {
                    return;
                }
                _salary = value;
                OnPropertyChanged("Salary");
            }
        }
        [Column(Name = "Position")]
        public string Position
        {
            get { return _position; }
            set
            {
                if (_position == value)
                {
                    return;
                }
                _position = value;
                OnPropertyChanged("Position");
            }
        }
        public Rights Right {get; set;}
        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged(string PropertyName = "")
        {
           PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(PropertyName));
        }
    }
}
