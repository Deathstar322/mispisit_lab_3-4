﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.Linq.Mapping;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace MISPISIT_lab_3_4.Models
{
    [Table(Name = "Rights")]
    internal class Rights : INotifyPropertyChanged
    {

        [Column(IsPrimaryKey = true, IsDbGenerated = true)]
        public int Id { get; set; }
        [Column(Name = "Dismiss_employees")]
        public byte DismissEmployees{ get; set; }
        [Column(Name = "Hire_employees")]
        public byte HireEmployees { get; set; }
        [Column(Name = "Edit_db")]
        public byte Editdb { get; set; }
        [Column(Name = "Employee_name")]
        public string EmployeeName { get; set; }

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged(string PropertyName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(PropertyName));
        }
    }
}
