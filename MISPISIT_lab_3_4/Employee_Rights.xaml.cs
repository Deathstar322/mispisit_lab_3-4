﻿using MISPISIT_lab_3_4.Models;
using System;
using System.Collections.Generic;
using System.Data.Linq;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace MISPISIT_lab_3_4
{
    /// <summary>
    /// Логика взаимодействия для Employee_Rights.xaml
    /// </summary>
    public partial class Employee_Rights : Window
    {
        static string connectionString = @"Data Source=localhost\SQLEXPRESS;Initial Catalog=Employees;Integrated Security=True";
        static DataContext database = new DataContext(connectionString);
        Employee emp = new Employee();
        Rights rights;
        public Employee_Rights(object employee)
        {
            emp = employee as Employee;
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            EmployeeName.Content += emp.Name;
            int rights_id = database.GetTable<Emp_Rights>().FirstOrDefault(x => x.EmployeeId == emp.Id).RightsId;
            rights = database.GetTable<Rights>().FirstOrDefault(x => x.Id == rights_id);
            if (rights.HireEmployees == 0)
            {
                Hire_emp_checkbox.IsChecked = false;
            }
            else
            {
                Hire_emp_checkbox.IsChecked = true;
            };
            if (rights.DismissEmployees == 0)
            {
                Dismiss_emp_checkbox.IsChecked = false;
            }
            else
            {
                Dismiss_emp_checkbox.IsChecked = true;
            };
            if (rights.Editdb == 0)
            {
                Edit_db_checkbox.IsChecked = false;
            }
            else
            {
                Edit_db_checkbox.IsChecked = true;
            };

        }

        private void Dismiss_emp_checkbox_Checked(object sender, RoutedEventArgs e)
        {
            rights.DismissEmployees = 1;
            database.SubmitChanges();
        }

        private void Dismiss_emp_checkbox_Unchecked(object sender, RoutedEventArgs e)
        {
            rights.DismissEmployees = 0;
            database.SubmitChanges();
        }

        private void Edit_db_checkbox_Checked(object sender, RoutedEventArgs e)
        {
            rights.Editdb = 1;
            database.SubmitChanges();
        }

        private void Edit_db_checkbox_Unchecked(object sender, RoutedEventArgs e)
        {
            rights.Editdb = 0;
            database.SubmitChanges();
        }

        private void Hire_emp_checkbox_Checked(object sender, RoutedEventArgs e)
        {
            rights.HireEmployees = 1;
            database.SubmitChanges();
        }

        private void Hire_emp_checkbox_Unchecked(object sender, RoutedEventArgs e)
        {
            rights.HireEmployees = 0;
            database.SubmitChanges();
        }
    }
}
