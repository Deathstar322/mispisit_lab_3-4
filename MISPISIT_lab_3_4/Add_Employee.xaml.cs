﻿using MISPISIT_lab_3_4.Models;
using System;
using System.Collections.Generic;
using System.Data.Linq;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace MISPISIT_lab_3_4
{
    /// <summary>
    /// Логика взаимодействия для Add_Employee.xaml
    /// </summary>
    public partial class Add_Employee : Window
    {
        static string connectionString = @"Data Source=localhost\SQLEXPRESS;Initial Catalog=Employees;Integrated Security=True";
        static DataContext database = new DataContext(connectionString);
        public Add_Employee()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Employee employee = new Employee {
            Name = Employee_Name_Box.Text,
            Salary = int.Parse(Employee_Salary_Box.Text),
            HireDate = DateTime.Parse(Employee_HireDate_Box.Text),
            Position = Employee_Position_Box.Text,
            };
            Rights right = new Rights
            {
                DismissEmployees = 0,
                Editdb = 0,
                HireEmployees = 0,
                EmployeeName = employee.Name,

            };
            database.GetTable<Rights>().InsertOnSubmit(right);
            database.SubmitChanges();
            database.GetTable<Employee>().InsertOnSubmit(employee);
            database.SubmitChanges();
            int emp_id = database.GetTable<Employee>().FirstOrDefault(x => x.Name == employee.Name).Id;
            int rights_id = database.GetTable<Rights>().FirstOrDefault(x => x.EmployeeName == employee.Name).Id;
            Emp_Rights emp_rights = new Emp_Rights
            {
                RightsId = rights_id,
                EmployeeId = emp_id,
            };
            database.GetTable<Emp_Rights>().InsertOnSubmit(emp_rights);
            database.SubmitChanges();
            (Application.Current.MainWindow as MainWindow).Update_DataGrid();
            this.Close();
        }
    }
}
